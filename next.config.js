/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  i18n:{
    locales: ['en', 'fr'],
    defaultLocale: 'fr',
  },
  noParse: content => {console.log(content); return false;}
}

module.exports = nextConfig
