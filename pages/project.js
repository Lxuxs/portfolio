import HeroProjects from '../components/projects/HeroProjects'
import Head from 'next/head'
export default function Project() {
  return (
    <div>
      <Head>
        <title>Projects - Louis Garrido Front-end & Web3 Developer</title>
        <meta name="description" content="Front-end & Web3 developer" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main>
        <HeroProjects/>
      </main>
    </div>
  )
}
