import Link from 'next/link'
import Image from 'next/image'
import Head from 'next/head'
import { useRouter } from 'next/router';
import Web2PEN from '../i18n/en/components/projects/Web2.json'
import Web2PFR from '../i18n/fr/components/projects/Web2.json'
import Web3PEN from '../i18n/en/components/projects/Web3.json'
import Web3PFR from '../i18n/fr/components/projects/Web3.json'
import Web3 from '../components/Interests/Web3'

export default function Web3Projects(){
  let router = useRouter();
  let txt = router.locale === 'en'? Web2PEN.dev
  : router.locale === 'fr'
  ? Web2PFR.dev
  : "";
  let done = router.locale === 'en'? Web3PEN.what
  : router.locale === 'fr'
  ? Web3PFR.quoi
  : "";
    return(
        <div className='snap-start w-screen h-screen flex flex-col items-center justify-center'>
          <Head>
            <title>Web 3 Projects - Louis Garrido Front-end & Web3 Developer</title>
            <meta name="description" content="Front-end & Web3 developer" />
            <link rel="icon" href="/favicon.png" />
          </Head>
          <div className="flex flex-row justify-between items-center py-8 border-solid border-b border-slate-400 w-10/12 fixed top-0">
            <div className='flex justify-center align-center items-center space-x-2'>
              <Link href='/' className='text-xl '>
                  <a>Louis Garrido</a>
              </Link>
              <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-cyan-500" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
              </svg>
            </div>
            <Link className='text-xlbg-sky-500/25 p-2 rounded-md' href='/project'>
              <a className='transition ease-in-out duration-500 hover:scale-125 underline decoration-cyan-300 underline-offset-4 decoration-2'>Projects</a>
            </Link>
          </div>
          <div className='flex flex-col justify-evenly items-center text-xl'>
            <h2 className='pb-4'>{txt}</h2>
            <ul className='text-center'>
              <li className='pb-2'><Link href='https://www.startweb3.xyz/' passHref><a target="_blank" rel="noopener noreferrer">- StartWeb3 ({done})</a></Link></li>
              <li className='pb-2'><Link href='https://swappy.vercel.app/' passHref><a target="_blank" rel="noopener noreferrer">- Swappy ({done})</a></Link></li>
              <li><Link href='https://tokence.vercel.app' passHref><a target="_blank" rel="noopener noreferrer">- Tokence ({done})</a></Link></li>
            </ul>
            <div className='flex flex-row justify-between items-center'>
              <div className='flex flex-col justify-around items-center'>
                <div className='flex flex-col'>
                  <div>
                    {/*Title and read whitepaper */}
                  </div>
                  <div>
                    {/* ETH graph */}
                  </div>
                  <div>
                    {/* Price Market cap change on 1 year */}
                  </div>
                </div>
                <div>
                  {/* Defintions and usefull links to start web3 and startweb3.xyz */}
                </div>
              </div>
              <div>
                {/* List of project with #  */}
              </div>
            </div>
            <div>
              {/*Slider of best web3 projects -> Developper DAO, Optimism, Arbitrum... (At least 5 différent projets)*/}
            </div>
          </div>
        </div>
    )
}