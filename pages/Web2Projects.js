import Link from 'next/link'
import Image from 'next/image'
import Head from 'next/head'
import { useRouter } from 'next/router';
import Web2PEN from '../i18n/en/components/projects/Web2.json'
import Web2PFR from '../i18n/fr/components/projects/Web2.json'

export default function Web2Projects(){
  let router = useRouter();
  let txt = router.locale === 'en'? Web2PEN.dev
  : router.locale === 'fr'
  ? Web2PFR.dev
  : "";
  return(
      <div className='snap-start w-screen h-screen flex flex-col items-center justify-center'>
        <Head>
          <title>Web 2 Projects - Louis Garrido Front-end & Web3 Developer</title>
          <meta name="description" content="Front-end & Web3 developer" />
          <link rel="icon" href="/favicon.png" />
        </Head>
        <div className="flex flex-row justify-between items-center py-8 border-solid border-b border-slate-400 w-10/12 fixed top-0">
          <div className='flex justify-center align-center items-center space-x-2'>
            <Link href='/' className='text-xl '>
                <a>Louis Garrido</a>
            </Link>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-cyan-500" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
              <path strokeLinecap="round" strokeLinejoin="round" d="M9 12l2 2 4-4M7.835 4.697a3.42 3.42 0 001.946-.806 3.42 3.42 0 014.438 0 3.42 3.42 0 001.946.806 3.42 3.42 0 013.138 3.138 3.42 3.42 0 00.806 1.946 3.42 3.42 0 010 4.438 3.42 3.42 0 00-.806 1.946 3.42 3.42 0 01-3.138 3.138 3.42 3.42 0 00-1.946.806 3.42 3.42 0 01-4.438 0 3.42 3.42 0 00-1.946-.806 3.42 3.42 0 01-3.138-3.138 3.42 3.42 0 00-.806-1.946 3.42 3.42 0 010-4.438 3.42 3.42 0 00.806-1.946 3.42 3.42 0 013.138-3.138z" />
            </svg>
          </div>
          <Link className='text-xlbg-sky-500/25 p-2 rounded-md' href='/project'>
            <a className='transition ease-in-out duration-500 hover:scale-125 underline decoration-cyan-300 underline-offset-4 decoration-2'>Projects</a>
          </Link>
        </div>
        <h2 className='text-xl pb-4'>{txt}</h2>
        <ul className='text-center text-xl'>
          <li className='pb-2 text-center'><Link href='https://xd.adobe.com/view/bc9036e4-fd7e-4b3f-4158-e4878e7de09b-caa1/' passHref ><a target="_blank" rel="noopener noreferrer">- HowFair</a></Link></li>
          <li className='text-center'><Link href='https://www.figma.com/file/91h5fEMKt8KSdTieZcBqjx/LP-URBANTRAIL---Maquette?node-id=0%3A1' passHref><a target="_blank" rel="noopener noreferrer">- Maquette Le Puy Urban Trail</a></Link></li>
        </ul>
      </div>
  )
}