import Head from 'next/head'
import About from '../components/About'
import Studypath from '../components/Studypath'
import Competence from '../components/Competence'
import Hero from '../components/Hero'
import Interest from '../components/Interest'

export default function Home() {
  
  return (
    <div className='flex-col items-center justify-center h-screen snap-y snap-mandatory overflow-scroll overflow-x-hidden scroll-smooth'>
      <Head>
        <title>Louis Garrido - Front-end & Web3 Developer</title>
        <meta name="description" content="Front-end & Web3 developer" />
        <link rel="icon" href="/favicon.png" />
      </Head>

      <main>
        <Hero/>
        <About/>
        <Studypath/>
        <Competence/>
        <Interest/>
      </main>
    </div>
  )
}
