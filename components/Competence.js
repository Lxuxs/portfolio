import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router';
import indexEN from '../i18n/en/index.json'
import indexFR from '../i18n/fr/index.json'

export default function Competence(){
  let router = useRouter();
  let h2 = router.locale === 'en'? indexEN.competence_h2
  : router.locale === 'fr'
  ? indexFR.competence_h2
  : "";
  let p1 = router.locale === 'en'? indexEN.competence_p1
  : router.locale === 'fr'
  ? indexFR.competence_p1
  : "";
  let solidity = router.locale === 'en'? indexEN.solidity
  : router.locale === 'fr'
  ? indexFR.solidity
  : "";
  let p2 = router.locale === 'en'? indexEN.compentence_p2
  : router.locale === 'fr'
  ? indexFR.compentence_p2
  : "";
  return (
      <div className=' lg:snap-start w-screen h-screen flex flex-col items-center justify-center lg:w-10/12 mx-auto'>
        <div className='lg:flex lg:flex-row flex flex-col-reverse items-center justify-center'>
          <div className='flex justify-center lg:w-2/4 w-9/12'>
            <Image src="/Competence.png" alt="competence tree" width={430} height={550}/>
          </div>
          <div className='flex-col lg:w-2/4 w-9/12 lg:my-auto mb-4'>
            <h2 className="text-center text-blue-h2 font-bold text-2xl lg:pb-8 tracking-wider">{h2}</h2>
            <p className='dark:text-white-txt-darkmode text-center lg:w-7/12 mx-auto lg:text-lg text-base lg:leading-9 leading-6 mt-4'>
              {p1}<span className="solidity">{solidity}</span>{p2}
            </p>
          </div>
        </div>
        
      </div>
  )
}