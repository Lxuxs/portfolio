export default function CryptoProjects(props){
    return(
        <div className='flex items-start h-28 mt-4 bg-Crypto1 bg-cover rounded-lg'>
            <div className="backdrop-blur-sm backdrop-contrast-75 w-full h-full rounded-lg">
                <div className="m-2">
                    <div className="text-slate-200 font-extrabold text-lg">
                        {props.name}
                    </div>
                    <div className="text-slate-200 font-bold text-sm">
                        {props.description}
                    </div>
                    <div className="flex flex-row items-center text-white font-black text-xs justify-end">
                        <div className="bg-neutral-800 p-1 px-2 rounded-xl tracking-wider">{props.state}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}