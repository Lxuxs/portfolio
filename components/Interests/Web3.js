import Image from 'next/image'
import CryptoProjects from './CryptoProjects'

export default function Web3(){
    return(
        <div>
            <div className='flex items-center align-center space-x-4'>
                <Image src="/Web3.svg" alt="web3 logo" width={32} height={32}/>
                <h3 className="text-xl">Web3</h3>
            </div>
            <div className='border-b border-slate-400'></div>
            <div className='grid grid-cols-4 gap-4'>
                <div className='w-full'>
                    <CryptoProjects name="NFT Project" state="Under development" description="NFT project with cartoon graphics, competing to earn model."/>
                </div>
                <div className='w-full'>
                    <div className='flex items-start h-28 mt-4 bg-slate-400 bg-cover rounded-lg'>
                        <div className="w-full h-full rounded-lg">
                            <div className="m-2">
                                <a href="https://www.startweb3.xyz/" className='text-white font-extrabold text-lg'>StartWeb3</a>
                                <div className="text-white font-bold text-sm">
                                    Generic website using react and ether.js about web3 information
                                </div>
                                <div className="flex flex-row text-white font-black text-xs justify-end">
                                    <div className="bg-neutral-800 p-1 px-2 rounded-xl tracking-wider">Live</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}