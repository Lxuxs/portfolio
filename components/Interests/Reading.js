import Books from './Books'
import BooksToRead from './BooksToRead'
import BooksReading from './BooksReading'
import { useRouter } from 'next/router';
import indexEN from '../../i18n/en/index.json'
import indexFR from '../../i18n/fr/index.json'

export default function Reading(){
    let router = useRouter();
    let h3 = router.locale === 'en'? indexEN.b_h3
    : router.locale === 'fr'
    ? indexFR.b_h3
    : "";
    let t1 = router.locale === 'en'? indexEN.b1_t
    : router.locale === 'fr'
    ? indexFR.b1_t
    : "";
    let a1 = router.locale === 'en'? indexEN.b1_a
    : router.locale === 'fr'
    ? indexFR.b1_a
    : "";
    let s1 = router.locale === 'en'? indexEN.b1_s
    : router.locale === 'fr'
    ? indexFR.b1_s
    : "";
    let t2 = router.locale === 'en'? indexEN.b2_t
    : router.locale === 'fr'
    ? indexFR.b2_t
    : "";
    let a2 = router.locale === 'en'? indexEN.b2_a
    : router.locale === 'fr'
    ? indexFR.b2_a
    : "";
    let s2 = router.locale === 'en'? indexEN.b2_s
    : router.locale === 'fr'
    ? indexFR.b2_s
    : "";
    let t3 = router.locale === 'en'? indexEN.b3_t
    : router.locale === 'fr'
    ? indexFR.b3_t
    : "";
    let a3 = router.locale === 'en'? indexEN.b3_a
    : router.locale === 'fr'
    ? indexFR.b3_a
    : "";
    let s3 = router.locale === 'en'? indexEN.b3_s
    : router.locale === 'fr'
    ? indexFR.b3_s
    : "";
    let t4 = router.locale === 'en'? indexEN.b4_t
    : router.locale === 'fr'
    ? indexFR.b4_t
    : "";
    let a4 = router.locale === 'en'? indexEN.b4_a
    : router.locale === 'fr'
    ? indexFR.b4_a
    : "";
    let s4 = router.locale === 'en'? indexEN.b4_s
    : router.locale === 'fr'
    ? indexFR.b4_s
    : "";
    let t5 = router.locale === 'en'? indexEN.b5_t
    : router.locale === 'fr'
    ? indexFR.b5_t
    : "";
    let a5 = router.locale === 'en'? indexEN.b5_a
    : router.locale === 'fr'
    ? indexFR.b5_a
    : "";
    let s5 = router.locale === 'en'? indexEN.b5_s
    : router.locale === 'fr'
    ? indexFR.b5_s
    : "";
    let t6 = router.locale === 'en'? indexEN.b6_t
    : router.locale === 'fr'
    ? indexFR.b6_t
    : "";
    let a6 = router.locale === 'en'? indexEN.b6_a
    : router.locale === 'fr'
    ? indexFR.b6_a
    : "";
    let s6 = router.locale === 'en'? indexEN.b6_s
    : router.locale === 'fr'
    ? indexFR.b6_s
    : "";
    let t7 = router.locale === 'en'? indexEN.b7_t
    : router.locale === 'fr'
    ? indexFR.b7_t
    : "";
    let a7 = router.locale === 'en'? indexEN.b7_a
    : router.locale === 'fr'
    ? indexFR.b7_a
    : "";
    let s7 = router.locale === 'en'? indexEN.b7_s
    : router.locale === 'fr'
    ? indexFR.b7_s
    : "";
    let t8 = router.locale === 'en'? indexEN.b8_t
    : router.locale === 'fr'
    ? indexFR.b8_t
    : "";
    let a8 = router.locale === 'en'? indexEN.b8_a
    : router.locale === 'fr'
    ? indexFR.b8_a
    : "";
    let s8 = router.locale === 'en'? indexEN.b8_s
    : router.locale === 'fr'
    ? indexFR.b8_s
    : "";
    let t9 = router.locale === 'en'? indexEN.b9_t
    : router.locale === 'fr'
    ? indexFR.b9_t
    : "";
    let a9 = router.locale === 'en'? indexEN.b9_a
    : router.locale === 'fr'
    ? indexFR.b9_a
    : "";
    let s9 = router.locale === 'en'? indexEN.b9_s
    : router.locale === 'fr'
    ? indexFR.b9_s
    : "";
    return(
        <div>
             <div className='flex items-center align-center space-x-4'>
                <svg xmlns="http://www.w3.org/2000/svg" className="lg:h-8 lg:w-8 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                    <path strokeLinecap="round" strokeLinejoin="round" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253" />
                </svg>
                <h3 className="text-xl">{h3}</h3>
            </div>
            <div className='border-b border-slate-400'></div>
            <div className='lg:grid lg:grid-cols-6 lg:gap-4 grid grid-cols-1 gap-2'>
                <BooksReading title={t1} author={a1} state={s1}/>
                <BooksReading title={t2} author={a2} state={s2}/>
                <BooksToRead title={t3} author={a3} state={s3}/>
                <BooksToRead title={t4} author={a4} state={s4}/>
                <BooksToRead title={t5} author={a5} state={s5}/>
                <BooksToRead title={t6} author={a6} state={s6}/>
                <BooksToRead title={t7} author={a7} state={s7}/>
                <Books title={t8} author={a8} state={s8}/>
                <Books title={t9} author={a9} state={s9}/>
            </div>
        </div>
    )
}