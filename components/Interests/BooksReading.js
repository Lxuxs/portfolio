export default function BooksReading(props){
    return(
        <div className='flex items-start h-24 mt-4'>
            <div className="bg-slate-100 w-full h-full rounded-lg">
                <div className="m-2">
                    <div className="text-gray font-black lg:text-sm text-xs">
                        {props.title},
                    </div>
                    <div className="text-gray font-bold text-xs py-2">
                        {props.author}
                    </div>
                    <div className="flex flex-row items-center text-white lg:font-black lg:text-xs justify-end">
                        <div className="bg-amber-400 lg:p-1 lg:px-2 p-1 px-2 text-xs rounded-xl tracking-wider">{props.state}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}