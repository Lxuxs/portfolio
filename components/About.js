import Link from 'next/link';
import { useRouter } from 'next/router';
import { PollingWatchKind } from 'typescript';
import indexEN from '../i18n/en/index.json'
import indexFR from '../i18n/fr/index.json'

export default function About(){
  let router = useRouter();
  let p1 = router.locale === 'en'? indexEN.about_p1
  : router.locale === 'fr'
  ? indexFR.a_propos_p1
  : "";
  let vue = router.locale === 'en'? indexEN.vue
  : router.locale === 'fr'
  ? indexFR.vue
  : "";
  let p2 = router.locale === 'en'? indexEN.about_p2
  : router.locale === 'fr'
  ? indexFR.a_propos_p2
  : "";
  let eth = router.locale === 'en'? indexEN.eth
  : router.locale === 'fr'
  ? indexFR.eth
  : "";
  let p3 = router.locale === 'en'? indexEN.about_p3
  : router.locale === 'fr'
  ? indexFR.a_propos_p3
  : "";
  let js = router.locale === 'en'? indexEN.js
  : router.locale === 'fr'
  ? indexFR.js
  : "";
  let p4 = router.locale === 'en'? indexEN.about_p4
  : router.locale === 'fr'
  ? indexFR.a_propos_p4
  : "";
  let react = router.locale === 'en'? indexEN.react
  : router.locale === 'fr'
  ? indexFR.react
  : "";
  let p5 = router.locale === 'en'? indexEN.about_p5
  : router.locale === 'fr'
  ? indexFR.a_propos_p5
  : "";
  let p6 = router.locale === 'en'? indexEN.about_p6
  : router.locale === 'fr'
  ? indexFR.a_propos_p6
  : "";
  let ici = router.locale === 'en'? indexEN.about_here
  : router.locale === 'fr'
  ? indexFR.ici_p6
  : "";
  let about_h2 = router.locale === 'en'? indexEN.about_h2
  : router.locale === 'fr'
  ? indexFR.a_propos_h2
  : "";
  let mail = router.locale === 'en'? indexEN.mail
  : router.locale === 'fr'
  ? indexFR.mail
  : "";
  let phone = router.locale === 'en'? indexEN.phone
  : router.locale === 'fr'
  ? indexFR.telephone
  : "";
  let urlpdf = router.locale === 'en'? indexEN.pdf
  : router.locale === 'fr'
  ? indexFR.pdf
  : "";
  return (
      <div className=' lg:snap-start w-screen h-screen lg:flex lg:flex-row flex flex-col-reverse items-center justify-center lg:w-10/12 mx-auto'>
        <div className='flex flex-col justify-center items-center lg:w-2/4 w-9/10 mx-auto'>
          <p className=' text-justify lg:w-10/12 w-9/12 lg:mb-12 font-medium lg:text-lg text-base lg:leading-9 leading-7'>
            {p1} 
            <span className="vue">{vue}</span>{p2}<span className="eth">{eth}</span>{p3}<span className="js">{js}</span>{p4}
            <span className="react">{react}</span>{p5}
          </p>
          <span className=' text-center lg:w-10/12 w-9/12 lg:font-medium lg:text-lg text-base lg:leading-9 leading-7'>
            {p6}<Link href={urlpdf}>{ici}</Link>
          </span> 
        </div>
        <div className='flex-col lg:w-2/4 lg:my-auto w-9/10'>
          <h2 className="text-center text-blue-h2 font-bold text-2xl lg:text-2xl lg:pb-8 tracking-wider mb-4">{about_h2}</h2>
          <div className='flex justify-center items-center mb-4'>
            <svg xmlns="http://www.w3.org/2000/svg" className="lg:h-6 lg:w-6 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
              <path strokeLinecap="round" strokeLinejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
            </svg>
            <span className=" pl-4 lg:text-lg">{mail}</span>
          </div>
          <div className='flex justify-center items-center mb-4 '>
            <svg xmlns="http://www.w3.org/2000/svg" className="lg:h-6 lg:w-6 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
              <path strokeLinecap="round" strokeLinejoin="round" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
            </svg>
            <span className=" pl-4 lg:text-lg">{phone}</span>
          </div>
        </div>
      </div>
  )
}