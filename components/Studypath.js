import Image from 'next/image'
import { useRouter } from 'next/router';
import indexEN from '../i18n/en/index.json'
import indexFR from '../i18n/fr/index.json'

export default function Studypath(){
    let router = useRouter();
    let p1 = router.locale === 'en'? indexEN.studypath_p1
    : router.locale === 'fr'
    ? indexFR.studypath_p1
    : "";
    let p2 = router.locale === 'en'? indexEN.studypath_p2
    : router.locale === 'fr'
    ? indexFR.studypath_p2
    : "";
    let p3 = router.locale === 'en'? indexEN.studypath_p3
    : router.locale === 'fr'
    ? indexFR.studypath_p3
    : "";
    let h2 = router.locale === 'en'? indexEN.studypath_h2
    : router.locale === 'fr'
    ? indexFR.studypath_h2
    : "";
    let p4 = router.locale === 'en'? indexEN.studypath_p4
    : router.locale === 'fr'
    ? indexFR.studypath_p4
    : "";
    return (
        <div className=' lg:snap-start w-screen h-screen lg:flex lg:flex-row flex flex-col-reverse items-center justify-center lg:w-10/12 lg:mx-auto'>
            <div className='flex flex-col justify-center items-center lg:w-2/4 w-9/10'>
                <p className=' text-center w-10/12  mx-auto lg:text-lg text-base lg:leading-9 leading-6'>{p1}</p>
                <div className='lg:my-4 my-2'><Image src="/Arrow.png" alt="arrow down" width={12} height={46}/></div>
                <p className=' text-center w-9/12 mx-auto lg:text-lg text-base lg:leading-9 leading-6'>{p2}</p>
                <div className='lg:my-4 my-2'><Image src="/Arrow.png" alt="arrow down" width={12} height={46}/></div>
                <p className=' text-center w-9/12 mx-auto lg:text-lg text-base lg:leading-9 leading-6'>{p3}</p>
            </div>
            <div className='flex-col lg:w-2/4 w-9/10 lg:my-auto'>
                <h2 className="text-center text-blue-h2 font-bold text-2xl pb-8 tracking-wider">{h2}</h2>
                <div className='flex justify-center items-center pb-6'>
                    <p className=' text-center lg:text-lg text-base lg:w-7/12  w-9/12 lg:leading-9 leading-7'>{p4}</p>
                </div>
            </div>
        </div>
    )
}