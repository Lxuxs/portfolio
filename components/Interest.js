import styles from '../styles/Home.module.css'
import Link from 'next/link'
import Travels from './Interests/Travels'
import Reading from './Interests/Reading'
import Web3 from './Interests/Web3'
import Image from 'next/image'
import { useRouter } from 'next/router';
import indexEN from '../i18n/en/index.json'
import indexFR from '../i18n/fr/index.json'

export default function Interest(){
    let router = useRouter();
    let h2 = router.locale === 'en'? indexEN.interest_h2
    : router.locale === 'fr'
    ? indexFR.interets_h2
    : "";
    let f1 = router.locale === 'en'? indexEN.footer_1
    : router.locale === 'fr'
    ? indexFR.footer_1
    : "";
    let f2 = router.locale === 'en'? indexEN.footer_2
    : router.locale === 'fr'
    ? indexFR.footer_2
    : "";
    return(
        <div className=' lg:snap-start w-screen lg:h-screen flex flex-col items-center justify-around'>
            <div className='w-10/12'>
                <h2 className='text-center text-blue-h2 lg:font-bold lg:text-2xl tracking-wider font-bold text-2xl'>{h2}</h2>
                <div className="pb-8 pt-4">
                    <Travels />
                </div>
                <div className="pb-8">
                    <Reading/>
                </div>
                <div className=' lg:py-0 py-12'></div>
            </div>
            <div className=" flex py-5 px-0 border-solid border-t border-slate-400 fixed bottom-0 w-10/12 bg-white">
                <a className=" flex justify-center items-start grow" href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app" target="_blank" rel="noopener noreferrer"> 
                    {f1}{' '}
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mx-1" viewBox="0 0 20 20" fill="currentColor">
                        <path fillRule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" clipRule="evenodd" />
                    </svg> {f2}
                </a>
            </div>
        </div>
    )
}